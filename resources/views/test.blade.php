<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Untitled Document</title>
</head>

<body>
    <table>
        <tr>
            <td>ردیف</td>
            <td>نام و نام خانوادگی</td>
            <td>شماره دانشجویی</td>
        </tr>
        @php($i=1)
        @foreach($names as $row)
        <tr>
            <td>{{$i}}</td>
            <td>{{$row->fname.' '.$row->lname}}</td>
            <td>{{$row->id_edu}}</td>
        </tr>
        @php($i++)
        @endforeach
    </table>
</body>

</html>