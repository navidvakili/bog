<!DOCTYPE html>
<html lang="en" dir="rtl">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>فرم ثبت نام</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <h1>فرم ثبت نام</h1>
    <form action="/form" method="post" enctype="multipart/form-data">
        @csrf
        <div class="container">
            @if($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                {{$error}} <br>
                @endforeach
            </div>
            @endif

            <div class="form-group row">
                <label for="fname" class="col-sm-2 col-form-label">* نام </label>
                <div class="col-sm-10">
                    <input type="text" name="fname" class="form-control" id="fname" autocomplete="off" value="{{ old('fname') }}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-2 col-form-label">نام خانوادگی</label>
                <div class="col-sm-10">
                    <input type="text" name="lname" class="form-control" id="lname" autocomplete="off" value="{{ old('lname')}}">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-2 col-form-label">عکس پرسنلی</label>
                <div class="col-sm-10">
                    <input type="file" name="photo" class="form-control" id="photo">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">ثبت نام</button>
        </div>
    </form>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>

</html>