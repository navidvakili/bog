<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        // $names = [
        //     [
        //         'fname' => 'علی',
        //         'lname' => 'اکبری',
        //         'id_edu' => '961122040'
        //     ],
        //     [
        //         'fname' => 'رضا',
        //         'lname' => 'حسینی',
        //         'id_edu' => '961122042'
        //     ],
        // ];

        // $names = Member::where('photo', 2)->first(); //limit(0,1)
        // $names = Member::where('photo', '>', 2)->get(); //limit(0,1)
        // $names = Member::where(['photo' => 2, 'fname' => 'سارا'])->get();
        // $names = Member::where(['fname'=>'سارا'])->orWhere(['fname'=>'رضیه'])->get();
        // $names = Member::where(['fname' => 'سارا', 'photo' => 2])->orWhere(['fname' => 'رضیه', 'photo' => 3])->toSql();
        // $names = Member::where(function ($query) {
        //     $query->where(['fname' => 'سارا', 'photo' => 2]);
        // })
        //     ->orWhere(function ($query) {
        //         $query->where(['fname' => 'رضیه', 'photo' => 3]);
        //     })->toSql();

        // $names = Member::orderBy('id', 'desc');
        // $count = $names->count();
        // $num_record = ($count > 2) ? round($count / 2) : $count;
        // $names = $names->limit($num_record)->get();
        // return $names;

        // $member = new Member();
        // $member->fname = "فاطمه سادات";
        // $member->lname = "میرشمسی";
        // $member->photo = 5;
        // $member->save();

        // $member = Member::create([
        //     'fname' => 'سید هادی',
        //     'lname' => 'رنجبر',
        //     'photo' => '4'
        // ]);

        // $member = Member::firstOrNew(['fname' => 'محمد مهدی', 'lname' => 'صادقیان'], ['photo' => 7]);
        // $member->save();
        // return $member;

        // $member = Member::find(1);
        // $member->lname = "اکبری تهرانی";
        // $member->save();


        // $member = Member::find(1)->update(['lname' => 'اکبری ونکورزاده']);

        $member = Member::find(1);
        if ($member !== null) $member->delete();
        else {
            abort('403', 'این بنده خدا پیدا نشد');
            // return 'این بنده خدا پیدا نشد';
        }

        $member->delete();


        // return view('test', compact('names'));
    }

    public function form()
    {
        return view('form');
    }

    public function store(Request $request)
    {
        $fname = $request->fname;
        $lname = $request->lname;
        $request->validate([
            'fname' => 'required',
            'photo' => 'required|image',
        ]);
        return $fname . ' ' . $lname;
    }
}
