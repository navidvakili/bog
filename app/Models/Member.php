<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    use HasFactory;
    protected $fillable = [
        'fname',
        'lname',
        'photo'
    ];
    // protected $table = 'my_contact';

    // protected $primaryKey = "user_id";
    // public $incrementing = false;
    // public $timestamps = false;
    // protected $casts = [
    //     'photo' => 'integer',
    // ];
}
